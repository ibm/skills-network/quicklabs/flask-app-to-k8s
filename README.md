# NOTICE
### This Project Was Migrated to author-gitlab.skills.network
#### Any changes you make here could result in either no effect or undesirable side effects.
#### Please Visit https://author-gitlab.skills.network/quicklabs/IBM-GPXX0QACEN

# Skills Network QuickLab Sample Project (**change me**)

Use this repository (GitLab project) as a refference when setting up your own tutorial or QuickLab project. When creating a repository for your QuickLab, you should create it by importing this starter project from a url:

[How to create your own project from this Starter Project](https://gph.is/g/aRWVMAJ)


## Publish

Since you used starter project as a basis for this project, GitLab CI will auto upload all content in this repository to the IBM Cloud Object Storage (COS) when you merge your files into `master` branch. All content will become publicly accessible via IBM COS, please check out the [section](#section) below how to get the public url to the resource.

## Links to your content

To use your content you are going to need a publicly accessible url (link). This link is created every time you merge your content in to master branch and it points to your content in the IBM Cloud Object Storage. You will find links to every piece of your content in the GitLab Page for your project. You will find your project's GitLab Page by going to the `Settings > Pages` menu or by using the this url  `<https://ibm-skills-network.gitlab.io/quicklabs/<project_name>`.

For example the GitLab Page for the sample project is at `<https://ibm-skills-network.gitlab.io/quicklabs/sample-project/>`.